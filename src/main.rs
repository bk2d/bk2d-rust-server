pub mod protos;

use crate::protos::{
    bk2d::*,
    bk2d_grpc::{create_bk2d, Bk2d},
};
use bk2d::BK2D;
use bk2d_mode::origin::{self, Origin};

use futures::{sync::oneshot, Future};
use grpcio::{Environment, RpcContext, RpcStatus, RpcStatusCode::*, ServerBuilder, UnarySink};
use std::{
    collections::BTreeMap,
    default::Default,
    io::{self, Read},
    sync::{Arc, Mutex},
    thread,
};
use uuid::Uuid;

struct Return<'a, T> {
    pub ctx: RpcContext<'a>,
    pub sink: UnarySink<T>,
}

impl<'a, T: protobuf::Message> Return<'a, T> {
    pub fn new(ctx: RpcContext<'a>, sink: UnarySink<T>) -> Self {
        Self { ctx, sink }
    }

    pub fn send_result(self, resp: T) {
        let f = self.sink.success(resp).map_err(|e| panic!(e));
        self.ctx.spawn(f);
    }

    pub fn send_err(self, err: RpcStatus) {
        let f = self.sink.fail(err).map_err(|e| panic!(e));
        self.ctx.spawn(f);
    }
}

macro_rules! grpc_unwrap {
    ($ret:ident, $exp:expr) => {{
        let res: Result<_, _> = $exp;
        match res {
            Result::Ok(x) => x,
            Err(err) => {
                return $ret.send_err(RpcStatus::new(InvalidArgument, Some(err.to_string())))
            }
        }
    }};
    ($ret:ident, $exp:expr, $status:expr) => {{
        let res: Result<_, _> = $exp;
        match res {
            Result::Ok(x) => x,
            Err(err) => return $ret.send_err($status(err)),
        }
    }};
}

macro_rules! get_username {
    ($ret:ident, $token:expr, $table:expr) => {{
        match $table.lock().unwrap().get($token) {
            Some(s) => s.to_string(),
            None => {
                return $ret.send_err(RpcStatus::new(
                    Unauthenticated,
                    Some("Token Not Found".to_string()),
                ))
            }
        }
    }};
}

macro_rules! sure_player {
    ($ret:ident, $bk2d:ident, $username:expr) => {
        if !$bk2d.is_start() || $bk2d.win().is_some() {
            $ret.send_err(RpcStatus::new(
                PermissionDenied,
                Some("Not Running".to_string()),
            ));
            return;
        }
        if $bk2d.current_player() != &$username {
            $ret.send_err(RpcStatus::new(
                PermissionDenied,
                Some("Not Your Turn".to_string()),
            ));
            return;
        };
    };
}

macro_rules! handler {
    ($name:ident ($self:ident)($ret:ident, $bk2d:ident) | $body:stmt) => {
        handler! { $name($self, _none_arg: None)($ret, $bk2d) | $body }
    };
    ($name:ident ($self:ident)($ret:ident, $bk2d:ident) -> $return:ty | $body:stmt) => {
        handler! { $name($self, _none_arg: None)($ret, $bk2d) -> $return | $body }
    };
    ($name:ident ($self:ident, $arg:ident : $argty:ty)($ret:ident, $bk2d:ident) | $body:stmt) => {
        handler! { $name($self, $arg: $argty)($ret, $bk2d) -> None | {
            $body;
            None::new()
        }}
    };
    ($name:ident ($self:ident, $arg:ident : $argty:ty)($ret:ident, $bk2d:ident) -> $return:ty | $body:stmt) => {
        fn $name(&mut $self, ctx: RpcContext<'_>, $arg: $argty, sink: UnarySink<$return>) {
            let $ret = Return::new(ctx, sink);
            let result = {
                #[allow(unused_mut)]
                let mut $bk2d = $self.bk2d.lock().unwrap();
                $body
            };
            $ret.send_result(result);
        }
    };
    ($name:ident ($self:ident, $arg:ident : $argty:ty)($ret:ident, $bk2d:ident, $username:ident) | $body:stmt) => {
        handler! { $name($self, $arg: $argty)($ret, $bk2d, $username) -> None | {
            $body;
            None::new()
        }}
    };
    ($name:ident ($self:ident, $arg:ident : $argty:ty)($ret:ident, $bk2d:ident, $username:ident) -> $return:ty | $body:stmt) => {
        handler! { $name($self, $arg: $argty)($ret, $bk2d) -> None | {
            let token = grpc_unwrap!($ret, $arg.token(), |_err| RpcStatus::new(
                Unauthenticated,
                Some("Token Not Found".to_string())
            ));
            let $username = get_username!($ret, &token, $self.tokens);
            sure_player!($ret, $bk2d, $username);
            $body
        }}
    };
}

#[derive(Clone)]
struct Bk2dService {
    tokens: Arc<Mutex<BTreeMap<Uuid, String>>>,
    bk2d: Arc<Mutex<Origin>>,
}

impl Bk2dService {
    pub fn new() -> Self {
        Bk2dService {
            tokens: Arc::new(Mutex::new(BTreeMap::new())),
            bk2d: Arc::new(Mutex::new(Origin::new(
                Default::default(),
                Default::default(),
            ))),
        }
    }
}

impl Bk2d for Bk2dService {
    handler! { join(self, player: Player)(ret, bk2d) -> Token | {
        let player: origin::Player = player.into();
        let name = player.name.clone();
        let token = Uuid::new_v4();
        {
            let mut tokens = self.tokens.lock().unwrap();
            let entry = tokens.entry(token);
            if let std::collections::btree_map::Entry::Occupied(..) = &entry {
                ret.send_err(RpcStatus::new(
                    PermissionDenied,
                    Some("Username Taken".to_string()),
                ));
                return;
            } else {
                entry.or_insert(name);
            }
        }
        grpc_unwrap!(ret, bk2d.add_player(player));
        token.into()
    }}

    handler! { start(self)(ret, bk2d) | {
        if bk2d.is_start() {
            ret.send_err(RpcStatus::new(InvalidArgument, Some("Can't start twice".to_string())));
            return;
        }
        bk2d.start();
    }}

    handler! { get_status(self)(ret, bk2d) -> Status | {
        use Status_Status::*;
        let mut resp = Status::new();
        let status = if bk2d.is_start() {
            if let Some(winner) = bk2d.win() {
                resp.set_player(winner.to_string());
                END
            } else {
                resp.set_player(bk2d.current_player().to_string());
                RUNNING
            }
        } else {
            START
        };
        resp.set_status(status);
        resp
    }}

    handler! { do_action(self, action: Action)(ret, bk2d, player) | {
        grpc_unwrap!(ret, bk2d.action(action.into()));
    }}

    handler! { next_player(self, token: Token)(ret, bk2d, player) | {
        bk2d.next_player();
    }}

    handler! { public_action(self, range: Range)(ret, bk2d) -> Publics | {
        let start = range.get_start().into();
        let end = if range.get_end() == 0 {
            None
        } else {
            Some(range.get_end().into())
        };
        bk2d
            .public_actions_from(start, end)
            .iter()
            .map(|x| x.to_owned().into())
            .collect()
    }}
}

fn main() {
    let env = Arc::new(Environment::new(4));
    let service = create_bk2d(Bk2dService::new());
    let mut server = ServerBuilder::new(env)
        .register_service(service)
        .bind("0.0.0.0", 2523)
        .build()
        .unwrap();
    server.start();

    let (tx, rx) = oneshot::channel();
    thread::spawn(move || {
        println!("Press ENTER to exit...");
        let _ = io::stdin().read(&mut [0]).unwrap();
        tx.send(())
    });
    let _ = rx.wait();
    let _ = server.shutdown().wait();
}
