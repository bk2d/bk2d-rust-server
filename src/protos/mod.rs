pub mod bk2d;
pub mod bk2d_grpc;

use ::bk2d::point;
use bk2d_mode::origin;
use uuid::Uuid;

macro_rules! impl_from {
    ($name:ident $path:tt | compact) => {
        impl_from! { self::bk2d::$name, $path::$name | compact }
    };
    ($from:ty, $to:ty | compact) => {
        impl From<$from> for $to {
            fn from(x: $from) -> $to {
                x.compact()
            }
        }
    };
}

impl bk2d::Path {
    pub fn compact(&self) -> point::Path {
        point::Path::new(self.get_dx(), self.get_dy())
    }
}

impl_from! { Path point | compact }

impl bk2d::Point {
    pub fn compact(&self) -> point::Point {
        point::Point::new(self.get_x(), self.get_y())
    }
}

impl_from! { Point point | compact }

impl From<point::Point> for bk2d::Point {
    fn from(x: point::Point) -> Self {
        let mut p = bk2d::Point::new();
        p.set_x(x.x);
        p.set_y(x.y);
        p
    }
}

impl bk2d::Token {
    pub fn token(&self) -> Result<Uuid, uuid::parser::ParseError> {
        Uuid::parse_str(self.get_token())
    }
}

impl From<uuid::Uuid> for bk2d::Token {
    fn from(x: uuid::Uuid) -> Self {
        let mut result = Self::new();
        result.set_token(x.to_string());
        result
    }
}

impl bk2d::Action {
    pub fn compact(&self) -> origin::Action {
        use self::bk2d::Action_Action as PA;
        use origin::Action as OA;
        let path = self.get_path().compact();
        match self.get_action() {
            PA::WALK => OA::Walk(path),
            PA::RUN => OA::Run(path),
            PA::ATTACK => OA::Attack(path),
        }
    }

    pub fn token(&self) -> Result<Uuid, uuid::parser::ParseError> {
        self.get_token().token()
    }
}

impl_from! { Action origin | compact }

impl bk2d::Player {
    pub fn compact(&self) -> origin::Player {
        origin::Player::new(
            self.get_username().to_string(),
            self.get_location().compact(),
        )
    }
}

impl_from! { Player origin | compact }

impl From<origin::PublicKind> for bk2d::Public_Kind {
    fn from(x: origin::PublicKind) -> Self {
        use self::bk2d::Public_Kind as BP;
        use origin::PublicKind as OP;
        match x {
            OP::Run => BP::RUN,
            OP::Attack => BP::ATTACK,
        }
    }
}

impl From<origin::Public> for bk2d::Public {
    fn from(x: origin::Public) -> Self {
        let mut r = bk2d::Public::new();
        r.set_id(x.id);
        r.set_round(x.round);
        r.set_location(x.data.into());
        r.set_kind(x.kind.into());
        r
    }
}

impl From<Vec<bk2d::Public>> for bk2d::Publics {
    fn from(x: Vec<bk2d::Public>) -> Self {
        let mut result = Self::new();
        result.set_publics(x.into());
        result
    }
}

impl std::iter::FromIterator<bk2d::Public> for bk2d::Publics {
    fn from_iter<I: IntoIterator<Item = bk2d::Public>>(iter: I) -> Self {
        let mut result = Self::new();
        result.set_publics(protobuf::RepeatedField::from_iter(iter));
        result
    }
}
